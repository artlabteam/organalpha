/* 
 * File:   Introitus.h
 * Author: artlab
 *
 * Created on 7 settembre 2014, 15.24
 */

#ifndef INTROITUS_H
#define	INTROITUS_H
#include <pthread.h>

#define NUM_THREADS  3
#define TCOUNT 10
#define COUNT_LIMIT 12

class Introitus {

    static int     count;
    static int     thread_ids[3];
    static pthread_mutex_t count_mutex;
    static pthread_cond_t count_threshold_cv;

    static void *inc_count(void *t);
    static void *watch_count(void *t);
public:
    void testPthread();
    Introitus();
    Introitus(const Introitus& orig);
    virtual ~Introitus();
private:

};

#endif	/* INTROITUS_H */

