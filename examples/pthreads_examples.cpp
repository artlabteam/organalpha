/* 
 * File:   Introitus.cpp
 * Author: artlab
 * 
 * Created on 7 settembre 2014, 15.24
 */

#include "pthread_examples.h"
#include <iostream>
#include <cmath>
#include <cstdlib>
using namespace std;

int     Introitus::count = 0;
int     Introitus::thread_ids[3] = {0,1,2};
pthread_mutex_t Introitus::count_mutex;
pthread_cond_t Introitus::count_threshold_cv;

void* Introitus::inc_count(void *t) 
{
  int i;
  long my_id = (long)t;
  timespec tim;
  tim.tv_nsec=1000;
  tim.tv_sec=0;

  for (i=0; i<TCOUNT; i++) {
    pthread_mutex_lock(&count_mutex);
    count++;

    /* 
    Check the value of count and signal waiting thread when condition is
    reached.  Note that this occurs while mutex is locked. 
    */
    if (count == COUNT_LIMIT) {
      pthread_cond_signal(&count_threshold_cv);
      cout << "inc_count(): thread " << my_id <<" count = " << count << "  Threshold reached." << endl;
      }
    cout << "inc_count(): thread " << my_id << ", count = " << count << ", I="<<i<<"-unlocking mutex" << endl;
    pthread_mutex_unlock(&count_mutex);

    /* Do some "work" so threads can alternate on mutex lock */
    nanosleep(&tim, &tim);
//    sleep(1);
    }
  pthread_exit(NULL);
}

void* Introitus::watch_count(void *t) 
{
  long my_id = (long)t;

  cout << "Starting watch_count(): thread " << my_id << endl;

  /*
  Lock mutex and wait for signal.  Note that the pthread_cond_wait 
  routine will automatically and atomically unlock mutex while it waits. 
  Also, note that if COUNT_LIMIT is reached before this routine is run by
  the waiting thread, the loop will be skipped to prevent pthread_cond_wait
  from never returning. 
  */
  pthread_mutex_lock(&count_mutex);
  while (count<COUNT_LIMIT) {
    pthread_cond_wait(&count_threshold_cv, &count_mutex);
    cout << "watch_count(): thread " << my_id << " Condition signal received." << endl;
    count += 125;
    cout << "watch_count(): thread " <<  my_id << " count now = " << count << endl;
    }
  pthread_mutex_unlock(&count_mutex);
  pthread_exit(NULL);
}
void Introitus::testPthread() {

  int i, rc;
  long t1=1, t2=2, t3=3;
  pthread_t threads[3];
  pthread_attr_t attr;

  /* Initialize mutex and condition variable objects */
  pthread_mutex_init(&count_mutex, NULL);
  pthread_cond_init (&count_threshold_cv, NULL);

  /* For portability, explicitly create threads in a joinable state */
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  pthread_create(&threads[0], &attr, watch_count, (void *)t1);
  pthread_create(&threads[1], &attr, inc_count, (void *)t2);
  pthread_create(&threads[2], &attr, inc_count, (void *)t3);

  /* Wait for all threads to complete */
  for (i=0; i<NUM_THREADS; i++) {
    pthread_join(threads[i], NULL);
  }
   cout << "Main(): Waited on " << NUM_THREADS << " threads. Done." << endl;

  /* Clean up and exit */
  pthread_attr_destroy(&attr);
  pthread_mutex_destroy(&count_mutex);
  pthread_cond_destroy(&count_threshold_cv);
  pthread_exit(NULL);
}
Introitus::Introitus() {
}

Introitus::Introitus(const Introitus& orig) {
}

Introitus::~Introitus() {
}

