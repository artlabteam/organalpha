/* 
 * File:   Midi.h
 * Author: artlab
 *
 * Created on 24 settembre 2014, 13.10
 */

#ifndef MIDI_H
#define	MIDI_H

#include "MidiDrv.h"

class Midi : MidiDrv{
public:
    Midi();
    Midi(const Midi& orig);
    virtual ~Midi();
private:

};

#endif	/* MIDI_H */

