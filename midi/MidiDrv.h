/* 
 * File:   MidiDrv.h
 * Author: artlab
 *
 * Created on 24 settembre 2014, 13.09
 */

#ifndef MIDIDRV_H
#define	MIDIDRV_H

#include <pthread.h>

class MidiDrv {
    pthread_t midiInPort;
    pthread_t midiOutPort;
public:
    MidiDrv();
    MidiDrv(const MidiDrv& orig);
    virtual ~MidiDrv();
    
private:

};

#endif	/* MIDIDRV_H */

